package ch.verttigo.craftok.tabcompl;

import net.md_5.bungee.api.plugin.Plugin;

public class PlayerTabComplete extends Plugin {

    @Override
    public void onEnable() {
        getProxy().getPluginManager().registerListener(this, new ListenerTabEvent());
    }
}
